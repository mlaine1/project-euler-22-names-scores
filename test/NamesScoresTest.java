import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Comparator;

import org.junit.Test;

import components.naturalnumber.NaturalNumber;
import components.naturalnumber.NaturalNumber2;
import components.queue.Queue;
import components.queue.Queue2;
import components.simplereader.SimpleReader;
import components.simplereader.SimpleReader1L;
import components.simplewriter.SimpleWriter;
import components.simplewriter.SimpleWriter1L;

public class NamesScoresTest {

    @Test
    public void testGetNamesFromFileEmptyFile() {
        String fileName = "data/EMPTY_FILE.txt";
        SimpleWriter outToFile = new SimpleWriter1L(fileName);
        SimpleReader inFile = new SimpleReader1L(fileName);
        String[] expectedOutput = new String[0];
        String[] actualOutput = NamesScores.getNamesFromFile(inFile);

        boolean arraysEqual = Arrays.equals(expectedOutput, actualOutput);
        inFile.close();
        outToFile.close();

        assertTrue(arraysEqual);

    }

    @Test
    public void testGetNamesFromFileOneName() {

        String fileName = "data/ONE_NAME.txt";
        SimpleWriter outToFile = new SimpleWriter1L(fileName);
        String name1 = "JOHN";
        outToFile.print(name1);
        SimpleReader inFile = new SimpleReader1L(fileName);
        String[] expectedOutput = { name1 };
        String[] actualOutput = NamesScores.getNamesFromFile(inFile);

        boolean arraysEqual = Arrays.equals(expectedOutput, actualOutput);
        inFile.close();
        outToFile.close();

        assertTrue(arraysEqual);

    }

    @Test
    public void testGetNamesFromFileTwoNames() {

        String fileName = "data/TWO_NAMES.txt";
        SimpleWriter outToFile = new SimpleWriter1L(fileName);
        String name1 = "JOHN";
        String name2 = "DOE";

        outToFile.print(name1 + "," + name2);
        SimpleReader inFile = new SimpleReader1L(fileName);
        String[] expectedOutput = { name1, name2 };
        String[] actualOutput = NamesScores.getNamesFromFile(inFile);

        boolean arraysEqual = Arrays.equals(expectedOutput, actualOutput);
        inFile.close();
        outToFile.close();

        assertTrue(arraysEqual);

    }

    @Test
    public void testGetNamesFromFileThreeNames() {

        String fileName = "data/THREE_NAMES.txt";
        SimpleWriter outToFile = new SimpleWriter1L(fileName);
        String name1 = "JOHN";
        String name2 = "DOE";
        String name3 = "JANE";

        outToFile.print(name1 + "," + name2 + "," + name3);
        SimpleReader inFile = new SimpleReader1L(fileName);
        String[] expectedOutput = { name1, name2, name3 };
        String[] actualOutput = NamesScores.getNamesFromFile(inFile);

        boolean arraysEqual = Arrays.equals(expectedOutput, actualOutput);
        inFile.close();
        outToFile.close();

        assertTrue(arraysEqual);

    }

    @Test
    public void testGetNamesAlphabeticalOrderEmptyNames() {
        Comparator<String> orderLessThan = new NamesScores.StringLessThan();
        String[] names = new String[0];
        Queue<String> expectedOutput = new Queue2<>();

        Queue<String> actualOutput = NamesScores
                .getNamesAlphabeticalOrder(names, orderLessThan);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetNamesAlphabeticalOrderOneName() {
        Comparator<String> orderLessThan = new NamesScores.StringLessThan();
        String name1 = "JANE";
        String[] names = { name1 };
        Queue<String> expectedOutput = new Queue2<>();
        expectedOutput.enqueue(name1);

        Queue<String> actualOutput = NamesScores
                .getNamesAlphabeticalOrder(names, orderLessThan);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetNamesAlphabeticalOrderTwoNamesInOrder() {
        Comparator<String> orderLessThan = new NamesScores.StringLessThan();
        String name1 = "JANE";
        String name2 = "JOHN";
        String[] names = { name1, name2 };
        Queue<String> expectedOutput = new Queue2<>();
        expectedOutput.enqueue(name1);
        expectedOutput.enqueue(name2);

        Queue<String> actualOutput = NamesScores
                .getNamesAlphabeticalOrder(names, orderLessThan);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetNamesAlphabeticalOrderTwoNamesNotInOrder() {
        Comparator<String> orderLessThan = new NamesScores.StringLessThan();
        String name1 = "JOHN";
        String name2 = "JANE";
        String[] names = { name1, name2 };
        Queue<String> expectedOutput = new Queue2<>();
        expectedOutput.enqueue(name2);
        expectedOutput.enqueue(name1);

        Queue<String> actualOutput = NamesScores
                .getNamesAlphabeticalOrder(names, orderLessThan);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetNamesAlphabeticalOrderThreeNamesNotInOrder() {
        Comparator<String> orderLessThan = new NamesScores.StringLessThan();
        String name1 = "JOHN";
        String name2 = "JANE";
        String name3 = "ZYRA";
        String[] names = { name1, name2, name3 };
        Queue<String> expectedOutput = new Queue2<>();
        expectedOutput.enqueue(name2);
        expectedOutput.enqueue(name1);
        expectedOutput.enqueue(name3);

        Queue<String> actualOutput = NamesScores
                .getNamesAlphabeticalOrder(names, orderLessThan);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetTotalNameScoreEmptyNames() {
        Queue<String> names = new Queue2<>();
        NaturalNumber expectedOutput = new NaturalNumber2();
        NaturalNumber actualOutput = NamesScores.getTotalNameScore(names);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetTotalNameScoreSingleName() {
        String name1 = "JANE";
        Queue<String> names = new Queue2<>();
        names.enqueue(name1);
        int name1Score = 30 * 1;
        int totalScore = name1Score;
        NaturalNumber expectedOutput = new NaturalNumber2(totalScore);

        NaturalNumber actualOutput = NamesScores.getTotalNameScore(names);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetTotalNameScoreTwoNames() {
        String name1 = "JANE";
        String name2 = "JOHN";
        Queue<String> names = new Queue2<>();
        names.enqueue(name1);
        names.enqueue(name2);
        int name1Score = 30 * 1;
        int name2Score = 47 * 2;
        int totalScore = name1Score + name2Score;
        NaturalNumber expectedOutput = new NaturalNumber2(totalScore);

        NaturalNumber actualOutput = NamesScores.getTotalNameScore(names);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetTotalNameScoreThreeNames() {
        String name1 = "AL";
        String name2 = "JANE";
        String name3 = "JOHN";
        Queue<String> names = new Queue2<>();
        names.enqueue(name1);
        names.enqueue(name2);
        names.enqueue(name3);
        int name1Score = 13 * 1;
        int name2Score = 30 * 2;
        int name3Score = 47 * 3;
        int totalScore = name1Score + name2Score + name3Score;
        NaturalNumber expectedOutput = new NaturalNumber2(totalScore);

        NaturalNumber actualOutput = NamesScores.getTotalNameScore(names);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetNameScoreEmptyName() {
        String name = "";
        String nameCount = "1";
        NaturalNumber namePosition = new NaturalNumber2(nameCount);
        NaturalNumber nameScore = new NaturalNumber2();
        NaturalNumber expectedOutput = new NaturalNumber2(nameScore);

        NaturalNumber actualOutput = NamesScores.getNameScore(name,
                namePosition);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetNameScoreNameValue19Position1() {
        String name = "BOB";
        int nameValueInt = 19;
        int nameCountInt = 1;
        int totalScore = nameValueInt * nameCountInt;
        NaturalNumber namePosition = new NaturalNumber2(nameCountInt);
        NaturalNumber expectedOutput = new NaturalNumber2(totalScore);

        NaturalNumber actualOutput = NamesScores.getNameScore(name,
                namePosition);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetNameScoreNameValue19Position2() {
        String name = "BOB";
        int nameValueInt = 19;
        int nameCountInt = 2;
        int totalScore = nameValueInt * nameCountInt;
        NaturalNumber namePosition = new NaturalNumber2(nameCountInt);
        NaturalNumber expectedOutput = new NaturalNumber2(totalScore);

        NaturalNumber actualOutput = NamesScores.getNameScore(name,
                namePosition);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetNameScoreNameValue35Position10() {
        String name = "ROB";
        int nameValueInt = 35;
        int nameCountInt = 10;
        int totalScore = nameValueInt * nameCountInt;
        NaturalNumber namePosition = new NaturalNumber2(nameCountInt);
        NaturalNumber expectedOutput = new NaturalNumber2(totalScore);

        NaturalNumber actualOutput = NamesScores.getNameScore(name,
                namePosition);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetLetterScoreLetterA() {
        char letter = 'A';
        int letterScore = 1;
        NaturalNumber expectedOutput = new NaturalNumber2(letterScore);
        NaturalNumber actualOutput = NamesScores.getLetterScore(letter);

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testGetLetterScoreLetterZ() {
        char letter = 'Z';
        int letterScore = 26;
        NaturalNumber expectedOutput = new NaturalNumber2(letterScore);
        NaturalNumber actualOutput = NamesScores.getLetterScore(letter);

        assertEquals(expectedOutput, actualOutput);
    }

}
