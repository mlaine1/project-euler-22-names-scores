Project Euler 22: Names Scores	      12/6/18

Problem Description
-----------------------------------------------------
Link: https://projecteuler.net/problem=22

Given a text file with (5000+) names sort the names into 
alphabetical order and get the score for each name and output
the total score for all the names. Each name score is calculated
as the total score of its letters * the number of the name as it
appears in alphabetical order) Letter score is calculated as: 
A = 1, B = 2 etc... 


Folder descriptions
-----------------------------------------------------
Project was made in eclipse

data folder contains 5 files: 
-p022_names.txt
	The given file containing the names separated by commas.

	4 Files created for test cases in data by NamesScoreTest.java 
	-EMPTY_FILE.txt
	-ONE_NAME.txt	
	-TWO_NAMES.txt
	-THREE_NAMES.txt
 		

src folder contains the program file: 
-NamesScore.java
	
	opens input stream to p022_names.txt data folder 
	
	the program file outputs to the console:
		-The total score of the names


test folder contains file with JUnit test cases: 
	-NamesScoreTest.java
		-Tests methods in NamesScore.java
		-creates and outputs to 4 files in data folder
		to run test cases on getNameFromFile method 		


NOTE: both NamesScore.java and NamesScoreTest.java use
imported components from OSU's API found at:
http://web.cse.ohio-state.edu/software/common/doc/


Program Usage
-----------------------------------------------------

Run NamesScore.java
-After program runs check console output for the total score: 871198282