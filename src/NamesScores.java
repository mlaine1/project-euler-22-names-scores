import java.util.Comparator;

import components.naturalnumber.NaturalNumber;
import components.naturalnumber.NaturalNumber2;
import components.queue.Queue;
import components.queue.Queue2;
import components.simplereader.SimpleReader;
import components.simplereader.SimpleReader1L;
import components.simplewriter.SimpleWriter;
import components.simplewriter.SimpleWriter1L;

public final class NamesScores {

    public static class StringLessThan implements Comparator<String> {
        @Override
        public final int compare(String first, String second) {
            return first.compareTo(second);
        }
    }

    private NamesScores() {
    }

    public static void main(String[] args) {
        String fileName = "data/p022_names.txt";
        SimpleReader inFile = new SimpleReader1L(fileName);
        SimpleWriter out = new SimpleWriter1L();

        Comparator<String> orderLessThan = new StringLessThan();

        String[] arrayOfNames = getNamesFromFile(inFile);

        Queue<String> namesInAlphabeticalOrder = getNamesAlphabeticalOrder(
                arrayOfNames, orderLessThan);

        NaturalNumber totalScore = getTotalNameScore(namesInAlphabeticalOrder);

        out.print("The total of all the name scores is: " + totalScore);

        inFile.close();
        out.close();
    }

    protected static String[] getNamesFromFile(SimpleReader inFile) {
        String names = "";
        if (!inFile.atEOS()) {
            names = inFile.nextLine();
            String[] namesArray = names.split(",");
            return namesArray;
        } else {
            String[] emptyArray = new String[0];
            return emptyArray;
        }

    }

    protected static Queue<String> getNamesAlphabeticalOrder(
            String[] arrayOfNames, Comparator<String> orderLessThan) {
        Queue<String> sorted = new Queue2<>();

        for (int i = 0; i < arrayOfNames.length; i++) {
            String temp = arrayOfNames[i];
            sorted.enqueue(temp);
        }
        sorted.sort(orderLessThan);
        return sorted;
    }

    protected static NaturalNumber getTotalNameScore(Queue<String> names) {

        NaturalNumber totalScore = new NaturalNumber2();
        NaturalNumber nameCount = new NaturalNumber2();

        while (names.length() > 0) {
            String name = names.dequeue();
            nameCount.increment();
            NaturalNumber nameScore = getNameScore(name, nameCount);
            totalScore.add(nameScore);
        }

        return totalScore;
    }

    protected static NaturalNumber getNameScore(String name,
            NaturalNumber nameCount) {
        NaturalNumber nameScore = new NaturalNumber2();

        for (int i = 0; i < name.length(); i++) {
            NaturalNumber letterScore = getLetterScore(name.charAt(i));
            nameScore.add(letterScore);
        }

        nameScore.multiply(nameCount);

        return nameScore;
    }

    protected static NaturalNumber getLetterScore(char letter) {
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int letterIndex = alphabet.indexOf(letter);
        letterIndex++;
        NaturalNumber letterScore = new NaturalNumber2(letterIndex);
        return letterScore;
    }
}
